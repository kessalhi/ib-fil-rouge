# IB-fil-rouge

## Clone le projet

- Cloner le projet dans le dossier de votre workspace eclipse.

<img src="docs/clone.png" alt="drawing" width="800"/>

## Importer le projet dans Eclipse

##### Imprter le projet dans eclipse en s�lectionnant "Project From Folder"

<img src="docs/import-new-project-1.png" alt="drawing" width="800"/>


##### Ensuite s�lectionner le chemin vers le dossier du projet (l� o� vous avez clon� le repository git)

<img src="docs/import-new-project-2.png" alt="drawing" width="800"/>

## Configurer le projet dans Eclipse

##### Les facets
<img src="docs/configure-facets.png" alt="drawing" width="800"/>


##### Le build path

1. 

<img src="docs/build-path-1.png" alt="drawing" width="800"/>

2.

<img src="docs/build-path-2.png" alt="drawing" width="800"/>

3.

<img src="docs/build-path-3.png" alt="drawing" width="800"/>
