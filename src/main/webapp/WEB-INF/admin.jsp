<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Administration</title>
</head>
<body>

<%@ include file="header.jspf" %>

<h1>Page d'administration</h1>

<table border="1">
	<tr>
		<th>Nom</th>
		<th>Email</th>
		<th>Mot de passe</th>
		<th>Date inscription</th>
		<th>Compteur blocage compte</th>
		<th>Date de fin de blocage</th>
	</tr>
	<c:forEach items="${utilisateurs}" var="utilisateurCourant">
		<tr>
			<td>${utilisateurCourant.nom}</td>
			<td>${utilisateurCourant.email}</td>
			<td>${utilisateurCourant.mdp}</td>
			<td>${utilisateurCourant.dateInscription}</td>
			<td>${utilisateurCourant.compteurBlocageCompte}</td>
			<td>${utilisateurCourant.dateFinBlocage}</td>
		</tr>
	</c:forEach>
</table>

</body>
</html>