<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Connexion</title>
</head>
<body>

<%@ include file="header.jspf" %>

<h1>Connexion</h1>

<form action="connexion" method="post">
	<label for="email">Email :</label>
	<input type="email" id="email" name="email" value="${param.email}"><br/>
	<label for="mdp">Mot de passe :</label>
	<input type="password" id="mdp" name="mdp"><br/>
	<input type="submit" value="se connecter" />
</form>
<p>${message}</p>
</body>
</html>