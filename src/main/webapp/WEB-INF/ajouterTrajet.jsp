<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ajouter un trajet</title>
</head>
<body>

<%@ include file="header.jspf" %>

<form action="ajouterTrajet" method="post">
	<label for="description">Description</label>
	<textarea rows="5" cols="20" name="description" id="description"></textarea><br/>
	<label for="villeDepart">Ville d�part</label>
	<input type="text" name="villeDepart" id="villeDepart"/><br/>
	<label for="villeArrivee">Ville arriv�e</label>
	<input type="text" name="villeArrivee" id="villeArrivee"/><br/>
	<label for="dateHeureDepart">Date/Heure d�part</label>
	<input type="datetime-local" name="dateHeureDepart" id="dateHeureDepart"/><br/>
	<label for="prix">prix</label>
	<input type="number" name="prix" id="prix" step="0.1"/><br/>
	<label for="nombrePlace">Nombre de places</label>
	<input type="number" name="nombrePlace" id="nombrePlace"/><br/>
	<input type="submit" value="valider"/>
</form>
</body>
</html>