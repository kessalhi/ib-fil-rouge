<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inscription</title>
</head>
<body>

<%@ include file="header.jspf" %>

<h1>Inscription</h1>

<form action="inscription" method="post">
	<label for="nom">Nom :</label>
	<input type="text" id="nom" name="nom" value="${param.nom}">${erreurs.get('nom')}<br/>
	<label for="email">Email :</label>
	<input type="email" id="email" name="email" value="${empty erreurs.get('email') ? param.email : ''}">${erreurs.get('email')}<br/>
	<label for="mdp">Mot de passe :</label>
	<input type="password" id="mdp" name="mdp">${erreurs.get('mdp')}<br/>
	<label for="confirmMdp">Confirmer le mot de passe :</label>
	<input type="password" id="confirmMdp" name="confirmMdp">${erreurs.get('confirmMdp')}<br/>
	<input type="submit" value="s'inscrire" />
</form>

</body>
</html>