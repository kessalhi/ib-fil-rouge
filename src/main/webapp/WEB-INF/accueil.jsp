<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Accueil</title>
</head>
<body>

<%-- <jsp:include page="header.jspf"></jsp:include> --%>
<%@ include file="header.jspf" %>

<h1>Bienvenue sur PapoteCar ! Le site de covoiturage malin !</h1>

<c:if test="${sessionScope.utilisateur != null}">
	<p>Vous �tes connect� en tant que ${sessionScope.utilisateur.nom}</p>
</c:if>

<c:if test="${sessionScope.utilisateur == null}">
	<p>Vous n'�tes pas connect�</p>
</c:if>

<form action="accueil" method="post">
	Choisir le filtre � appliquer :
	<label for="villeDepart">Ville d�part</label>
	<input type="text" name="villeDepart" id="villeDepart"/><br/>
	<label for="villeArrivee">Ville arriv�e</label>
	<input type="text" name="villeArrivee" id="villeArrivee"/><br/>
	<label for="dateDepart">Date d�part</label>
	<input type="date" name="dateDepart" id="dateDepart"/><br/>
	<input type="submit" value="filtrer"/>
</form>

<c:forEach items="${trajets}" var="trajet">
	<p>${trajet.conducteur.nom} va � ${trajet.villeArrivee} en partant de ${trajet.villeDepart}</p>
</c:forEach>

</body>
</html>