package fr.ibcegos.papotecar.servlets;

import java.io.IOException;
import java.util.List;

import fr.ibcegos.papotecar.modele.managers.UtilisateurManager;
import fr.ibcegos.papotecar.modele.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/administration")
public class Administration extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// on va chercher en BDD la liste des utilisateurs
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		List<Utilisateur> utilisateurs = utilisateurManager.findAll();
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		request.setAttribute("utilisateurs", utilisateurs);
		this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
