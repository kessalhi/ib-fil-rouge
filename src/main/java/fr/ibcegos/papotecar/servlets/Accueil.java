package fr.ibcegos.papotecar.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import fr.ibcegos.papotecar.modele.managers.TrajetManager;
import fr.ibcegos.papotecar.modele.objets.Trajet;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/accueil")
public class Accueil extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		TrajetManager trajetManager = new TrajetManager();
		List<Trajet> trajets = trajetManager.findAll();
		
		request.setAttribute("trajets", trajets);
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String villeDepart = request.getParameter("villeDepart");
		String villeArrivee = request.getParameter("villeArrivee");
		LocalDate dateDepart = null;
		System.out.println(request.getParameter("dateDepart"));
		if(request.getParameter("dateDepart").length() > 0) {
			dateDepart = 
					LocalDate.parse(request.getParameter("dateDepart")); 
		}
		
		
		TrajetManager trajetManager = new TrajetManager();
		
		List<Trajet> trajets = trajetManager.findAllFilterBy(villeDepart, villeArrivee, dateDepart);
		request.setAttribute("trajets", trajets);
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
		.forward(request, response);
	}

}
