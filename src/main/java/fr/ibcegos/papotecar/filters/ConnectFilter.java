package fr.ibcegos.papotecar.filters;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebFilter(urlPatterns = {"/connect/*"})
public class ConnectFilter implements Filter {

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, 
			FilterChain filterChain)
			throws IOException, ServletException {
		
		// je cast mes 2 premiers param�tres
		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
		
		if(httpServletRequest.getSession().getAttribute("utilisateur") != null) {
			// laisser passer la requete
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			// si on est pas connect� alors on redirige vers l'accueil
			// une autre fa�on de rediriger vers une servlet
			httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/accueil");
		}

		
	}

}
