package fr.ibcegos.papotecar.modele.objets;

import java.time.LocalDateTime;

public class Trajet {
	
	private int id;
	private String description;
	private String villeDepart;
	private String villeArrivee;
	private LocalDateTime dateHeureDepart;
	private double prix;
	private int nombreDePlace;
	private Utilisateur conducteur;

	public Trajet() {
		// TODO Auto-generated constructor stub
	}

	public Trajet(String description, String villeDepart, String villeArrivee, LocalDateTime dateHeureDepart,
			double prix, int nombreDePlace, Utilisateur conducteur) {
		super();
		this.description = description;
		this.villeDepart = villeDepart;
		this.villeArrivee = villeArrivee;
		this.dateHeureDepart = dateHeureDepart;
		this.prix = prix;
		this.nombreDePlace = nombreDePlace;
		this.conducteur = conducteur;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVilleDepart() {
		return villeDepart;
	}

	public void setVilleDepart(String villeDepart) {
		this.villeDepart = villeDepart;
	}

	public String getVilleArrivee() {
		return villeArrivee;
	}

	public void setVilleArrivee(String villeArrivee) {
		this.villeArrivee = villeArrivee;
	}

	public LocalDateTime getDateHeureDepart() {
		return dateHeureDepart;
	}

	public void setDateHeureDepart(LocalDateTime dateHeureDepart) {
		this.dateHeureDepart = dateHeureDepart;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getNombreDePlace() {
		return nombreDePlace;
	}

	public void setNombreDePlace(int nombreDePlace) {
		this.nombreDePlace = nombreDePlace;
	}

	public Utilisateur getConducteur() {
		return conducteur;
	}

	public void setConducteur(Utilisateur conducteur) {
		this.conducteur = conducteur;
	}

	@Override
	public String toString() {
		return "Trajet [id=" + id + ", description=" + description + ", villeDepart=" + villeDepart + ", villeArrivee="
				+ villeArrivee + ", dateHeureDepart=" + dateHeureDepart + ", prix=" + prix + ", nombreDePlace="
				+ nombreDePlace + ", conducteur=" + conducteur + "]";
	}

}
