package fr.ibcegos.papotecar.modele.objets;

import java.time.LocalDate;

public class Utilisateur {
	
	private int id; // car on enregistre un utilisateur en BDD
	private String nom;
	private String email;
	private String mdp;
	private LocalDate dateInscription;
	private int compteurBlocageCompte;
	private LocalDate dateFinBlocage;
	
	public Utilisateur() {
		// TODO Auto-generated constructor stub
	}

	public Utilisateur(String nom, String email, String mdp) {
		this.nom = nom;
		this.email = email;
		this.mdp = mdp;
		this.dateInscription = LocalDate.now();
		this.compteurBlocageCompte = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public LocalDate getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(LocalDate dateInscription) {
		this.dateInscription = dateInscription;
	}

	public int getCompteurBlocageCompte() {
		return compteurBlocageCompte;
	}

	public void setCompteurBlocageCompte(int compteurBlocageCompte) {
		this.compteurBlocageCompte = compteurBlocageCompte;
	}

	public LocalDate getDateFinBlocage() {
		return dateFinBlocage;
	}

	public void setDateFinBlocage(LocalDate dateFinBlocage) {
		this.dateFinBlocage = dateFinBlocage;
	}

	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", email=" + email + ", mdp=" + mdp + ", dateInscription="
				+ dateInscription + ", compteurBlocageCompte=" + compteurBlocageCompte + ", dateFinBlocage="
				+ dateFinBlocage + "]";
	}

}
