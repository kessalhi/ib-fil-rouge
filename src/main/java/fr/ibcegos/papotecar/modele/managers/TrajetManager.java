package fr.ibcegos.papotecar.modele.managers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.papotecar.modele.dao.TrajetDAO;
import fr.ibcegos.papotecar.modele.objets.Trajet;

public class TrajetManager {

	public void enregistrer(Trajet trajet) {
		TrajetDAO trajetDAO = new TrajetDAO();
		trajetDAO.enregistrer(trajet);
	}

	public List<Trajet> findAll() {
		TrajetDAO trajetDAO = new TrajetDAO();
		return trajetDAO.findAll();
	}

	public List<Trajet> findAllFilterBy(String villeDepart, String villeArrivee, LocalDate dateDepart) {
		List<Trajet> trajets = findAll();
		
		List<Trajet> trajetsFiltre1 = new ArrayList<>();
		for (Trajet trajet : trajets) {
			if(villeDepart.isEmpty() || trajet.getVilleDepart().equals(villeDepart)) {
				trajetsFiltre1.add(trajet);
			}
		}
		
		List<Trajet> trajetsFiltre2 = new ArrayList<>();
		for (Trajet trajet : trajetsFiltre1) {
			if(villeArrivee.isEmpty() || trajet.getVilleArrivee().equals(villeArrivee)) {
				trajetsFiltre2.add(trajet);
			}
		}

		return trajetsFiltre2;
	}

}
