package fr.ibcegos.papotecar.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.papotecar.modele.objets.Utilisateur;

public class UtilisateurDAO {
	
	public void enregistrer(Utilisateur utilisateur) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			/*Statement statement = connexion.createStatement();
			String query = "INSERT INTO Utilisateur(nom, email, mdp) VALUES('"
					+ utilisateur.getNom() + "','" + utilisateur.getEmail() 
					+ "', CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast('" + utilisateur.getMdp() + "'as varchar)),2)" 
					+ ");";
			System.out.println(query);
			// 3 - Ex�cuter la requete
			statement.executeUpdate(query);*/
			String query = "INSERT INTO Utilisateur(nom, email, mdp, dateInscription) VALUES(?,?,CONVERT(NVARCHAR(32),"
					+ "HASHBYTES('MD5', cast(? as varchar)),2),?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, utilisateur.getNom());
			// le nom de l'utilisateur est mis en tant que 
			// string dans le 1er parametre de la query
			pstmt.setString(2, utilisateur.getEmail());
			pstmt.setString(3, utilisateur.getMdp());
			pstmt.setDate(4, Date.valueOf(utilisateur.getDateInscription()));
			
			//pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Utilisateur findByEmailAndMdp(String email, String mdp) {
		Utilisateur utilisateur = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM Utilisateur WHERE email = ? and "
					+ "mdp = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, email);
			pstmt.setString(2, mdp);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			// 4 - G�rer les retours �ventuels
			while(rs.next()) {
				utilisateur = remplirUtilisateur(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}

	public void updateCompteurAndDateDeBlocage(String email) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "UPDATE Utilisateur SET compteurBlocageCompte = compteurBlocageCompte + 1"
					+ " WHERE email = ? and compteurBlocageCompte < 5";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, email);
			pstmt.executeUpdate();
			
			query = "SELECT * from UTILISATEUR where email = ?";
			pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();
			int compteurBlocageCompte = 0;
			while(rs.next()) {
				compteurBlocageCompte = rs.getInt("compteurBlocageCompte");
			}
			if(compteurBlocageCompte == 5) {
				query = "UPDATE Utilisateur SET dateFinBlocage = ? where email = ?";
				pstmt = connexion.prepareStatement(query);
				pstmt.setDate(1, Date.valueOf(LocalDate.now().plusDays(1)));
				pstmt.setString(2, email);
				pstmt.executeUpdate();
			}
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void resetCompteurAndDateDeBlocage(String email) {
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "UPDATE Utilisateur SET compteurBlocageCompte = 0, dateFinBlocage = null WHERE email = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, email);
			// 3 - Ex�cuter la requete
			pstmt.executeUpdate();
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Utilisateur> findAll() {
		
		List<Utilisateur> utilisateurs = new ArrayList<>();
		
		// 1 - R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 - Fabriquer la requete
		String query = "SELECT * FROM Utilisateur";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Utilisateur utilisateur = remplirUtilisateur(rs);
				utilisateurs.add(utilisateur);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return utilisateurs;
	}
	
	public static Utilisateur remplirUtilisateur(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt("id"));
		utilisateur.setNom(rs.getString("nom"));
		utilisateur.setEmail(rs.getString("email"));
		utilisateur.setMdp(rs.getString("mdp")); // le mdp stock� est hach� ici
		utilisateur.setCompteurBlocageCompte(rs.getInt("compteurBlocageCompte"));
		if(rs.getDate("dateFinblocage") != null) {
			utilisateur.setDateFinBlocage(rs.getDate("dateFinblocage").toLocalDate());					
		}
		utilisateur.setDateInscription(rs.getDate("dateInscription").toLocalDate());
		return utilisateur;
	}


}
