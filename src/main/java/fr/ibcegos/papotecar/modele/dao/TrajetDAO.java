package fr.ibcegos.papotecar.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.papotecar.modele.objets.Trajet;
import fr.ibcegos.papotecar.modele.objets.Utilisateur;

public class TrajetDAO {

	public void enregistrer(Trajet trajet) {
		// 1 - Se connecter � la BDD
		Connection cnx = PoolConnexion.getConnexion();
		// 2 - Fabriquer la requete
		String query = "INSERT INTO Trajet(description, villeDepart, villeArrivee, "
				+ "dateHeureDepart, prix, nombreDePlace, conducteur) "
				+ "VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			pstmt.setString(1, trajet.getDescription());
			pstmt.setString(2, trajet.getVilleDepart());
			pstmt.setString(3, trajet.getVilleArrivee());
			pstmt.setTimestamp(4, Timestamp.valueOf(trajet.getDateHeureDepart()));
			pstmt.setDouble(5, trajet.getPrix());
			pstmt.setInt(6, trajet.getNombreDePlace());
			pstmt.setInt(7, trajet.getConducteur().getId());
			pstmt.executeUpdate();
			cnx.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	public List<Trajet> findAll() {
		List<Trajet> trajets = new ArrayList<>();
		// 1 - Se connecter � la BDD
		Connection cnx = PoolConnexion.getConnexion();
		// 2 - Fabrique la requete
		String query = "SELECT t.id as id_trajet, u.id as id_conducteur, * FROM Trajet t JOIN Utilisateur u ON t.conducteur = u.id";
		try {
			PreparedStatement pstmt = cnx.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Trajet trajet = new Trajet();
				trajet.setId(rs.getInt("id_trajet"));
				trajet.setDescription(rs.getString("description"));
				trajet.setVilleDepart(rs.getString("villeDepart"));
				trajet.setVilleArrivee(rs.getString("villeArrivee"));
				trajet.setDateHeureDepart(rs.getTimestamp("dateHeureDepart").toLocalDateTime());
				trajet.setPrix(rs.getDouble("prix"));
				trajet.setNombreDePlace(rs.getInt("nombreDePlace"));
				Utilisateur conducteur = UtilisateurDAO.remplirUtilisateur(rs);
				trajet.setConducteur(conducteur);
				trajet.getConducteur().setId(rs.getInt("id_conducteur"));
				trajets.add(trajet);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return trajets;
	}

}
